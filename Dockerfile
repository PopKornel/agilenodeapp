FROM node:18-alpine

WORKDIR /app

COPY package.json .

RUN npm i

COPY . .

# RUN yarn install --production
CMD ["node", "./index.js"]

EXPOSE 8081 