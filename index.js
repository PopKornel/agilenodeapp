const express = require('express');
const session = require('express-session');
const app = express();

// logger
var logger = require('morgan');
var fs = require('fs');
var dir = './logs';

// create logs directory
if (!fs.existsSync(dir)){
  fs.mkdirSync(dir);
}
// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(`./${dir}/access.log`, { flags: 'a' })
app.use(logger({ stream: accessLogStream }))

app.use(express.urlencoded({ extended: true }));
app.use(session({
  secret: 'secretKey',
  resave: false,
  saveUninitialized: true
}));

const users = [
  { username: 'user1', password: 'password1' },
  { username: 'user2', password: 'password2' }
];

app.get('/', (req, res) => {

  res.send('Welcome to the home page.');
});

app.get('/login', (req, res) => {
  res.send(`
    <form action="/login" method="post">
      <label for="username">Username:</label>
      <input type="text" id="username" name="username">
      <br><br>
      <label for="password">Password:</label>
      <input type="password" id="password" name="password">
      <br><br>
      <input type="submit" value="Submit">
    </form>
  `);
});

app.post('/login', (req, res) => {
  const { username, password } = req.body;
  const user = users.find(u => u.username === username && u.password === password);
  if (user) {
    req.session.user = user;
    res.redirect('/');
  } else {
    res.send('Invalid username or password.');
  }
});

app.get('/logout', (req, res) => {
  myLogger.log("Welcome to logout page(get request for '\logout' )")
  req.session.destroy(err => {
    if (err) {
      res.send(err);
    } else {
      res.redirect('/login');
    }
  });
});

// module.exports = app;

app.listen(8081, () => {
  console.log('Server started on port 8081');
});
